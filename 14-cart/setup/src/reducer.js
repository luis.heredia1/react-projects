const reducer = (state, action) => {
  switch (action.type) {
    case "CLEAR_CART":
      return { ...state, cart: [] };
    case "REMOVE":
      return {
        ...state,
        cart: state.cart.filter((cartItem) => cartItem.id !== action.payload),
      };
    case "INCREASE":
      let increasedCart = state.cart.map((cartItem) => {
        if (cartItem.id === action.payload) {
          return { ...cartItem, amount: cartItem.amount + 1 };
        }
        return cartItem;
      });

      return { ...state, cart: increasedCart };

    case "DECREASE":
      let decreasedCart = state.cart
        .map((cartItem) => {
          if (cartItem.id === action.payload) {
            return { ...cartItem, amount: cartItem.amount - 1 };
          }
          return cartItem;
        })
        .filter((cartItem) => cartItem.amount !== 0);
      return { ...state, cart: decreasedCart };

    case "GET_TOTALS":
      let { total, amount } = state.cart.reduce(
        (cartTotal, cartItem) => {
          const { price, amount } = cartItem;
          const itemTotal = price * amount;
          cartTotal.total += itemTotal;
          cartTotal.amount += amount;
          return cartTotal;
        },
        {
          total: 0,
          amount: 0,
        }
      );
      total = parseFloat(total.toFixed(2));

      return { ...state, total, amount };

    case "LOADING":
      return { ...state, loading: true };

    case "DISPLAY_ITEMS":
      return { ...state, cart: action.payload, loading: false };

    case "TOGGLE_AMOUNT":
      const cartItem = state.cart.find((item) => item.id === action.payload.id);
      if (!cartItem) {
        return state;
      }

      const newCart = state.cart.map((item) => {
        if (item.id === cartItem.id) {
          switch (action.payload.type) {
            case "inc":
              return { ...item, amount: item.amount + 1 };
            case "dec":
              return { ...item, amount: item.amount - 1 };
            default:
              return item;
          }
        }
        return item;
      });
      const updatedCart = newCart.filter((item) => item.amount > 0);
      return { ...state, cart: updatedCart };
    default:
      break;
  }
  return state;
};

export default reducer;
