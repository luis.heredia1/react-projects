import React, { useState } from 'react';
import data from './data';
import Question from './Question';
import SingleQuestion from './Question';

function App() {
  const [questions, setQuestions] = useState(data);
  return(
    <main className='main'>
      <div className='container'>
        <h3 className='container-h3'> Questions and anwsers about Login </h3>
        <div className='info'>
          {
            questions.map((question) => {
              return <Question question = { question.id } { ...question }/>

            })
          }
        </div>
      </div>
    </main>
  )
}

export default App;
