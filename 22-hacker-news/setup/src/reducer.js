import {
  SET_LOADING,
  SET_STORIES,
  REMOVE_STORY,
  HANDLE_PAGE,
  HANDLE_SEARCH,
} from "./actions";

const reducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_LOADING:
      return { ...state, isLoading: true };
    case SET_STORIES:
      return {
        ...state,
        isLoading: false,
        hits: payload.hits,
        nbPages: payload.nbPages,
      };

    case REMOVE_STORY:
      return {
        ...state,
        hits: state.hits.filter((story) => story.objectID !== payload),
      };
    case HANDLE_SEARCH:
      return { ...state, query: payload, page:0}
    case HANDLE_PAGE:
      if(payload === 'asc'){
        let newPage = state.page + 1
        
        if(newPage > state.nbPages -1 ){
          newPage = 0;
        }
        return {...state, page: newPage}

      }

      if(payload === 'dec'){
        let newPage = state.page - 1
        
        if(newPage < 0 ){
          newPage = state.nbPages - 1;
        }
        return {...state, page: newPage}

      }

    default:
      throw new Error(`no matching ${type} type`);
  }
};
export default reducer;
